import React from "react";
import ListOfItems from "./ListOfItems";

export default function Favourites(props) {
  let filteredList = props.list.filter(function(item) {
    return props.favourites.includes(item.name);
  });

  return (
    <div>
      {" "}
      <ListOfItems
        favourites={props.favourites}
        className="list"
        list={filteredList}
        isAddToCartButton = {true}
      />
    </div>
  );
}
