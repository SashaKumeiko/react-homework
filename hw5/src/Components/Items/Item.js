import React from 'react';
import Button from '../Buttons/Button';
import '../../Styles/item.scss';

function Item(props) {

  return (
    <div className={props.className}>
      <div className="item-top">
        <span
          className={props.hasCloseButtonClass}
          onClick={props.removeFromCart}
          data-name={props.details.name}
        >
          {props.hasX}
        </span>
      </div>
      <img src={props.details.image} />

      <div className="item-name">
        <span>{props.details.name}</span>
      </div>
      <div className="color">color: {props.details.color}</div>
      <div className="favourite">
        <div>
          <span>
            <i
              className={props.starClassName}
              data-name={props.details.name}
              onClick={() => props.addToFav(props)}
            ></i>
          </span>
        </div>
        <div>
          <span>product code: {props.details['product code']}</span>
        </div>
      </div>

      <div className="price">
        <span>{props.details.price} UAH</span>
        {props.isAddToCartButton && (
          <Button
            className="first-button button"
            // onClick={()=>props.setName(props.details.name)}
            onClick={()=>props.checkModal(props)}
            // onClick={checkModal}
            text="add to cart"
            background={{backgroundColor: '#00B1E1'}}
          />
        )}
      </div>

    </div>
  );
}

export default Item;
