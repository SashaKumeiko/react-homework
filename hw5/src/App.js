import React, {useEffect} from 'react';
import ListOfItems from './Components/Items/ListOfItems';
import Cart from './Components/Items/Cart';
import Favourites from './Components/Items/Favourites';
import {Route, Link, Switch} from 'react-router-dom';
import {getItems, cartItemsAction, favouritesAction} from './store/actions/getItems';
import {useSelector, useDispatch} from 'react-redux';

import './Styles/app.scss';

function App(props) {
  const items = useSelector(state => state.itemsReducer.items);
  const favourites = useSelector(state => state.listsReducer.favouritesFromLs);
  const cartItems = useSelector(state => state.listsReducer.cartItems);
  
  const dispatch = useDispatch();
 

  useEffect(() => {
    if (!localStorage.getItem('cart')) {
      localStorage.setItem('cart', '');
    }

    if (!localStorage.getItem('favourites')) {
      localStorage.setItem('favourites', '');
    }
    dispatch(getItems())
    dispatch(cartItemsAction());
    dispatch(favouritesAction());

  }, []);

  return (
    <div>
      {items.length && (
        <div>
          <nav className="navigation">
            <Link to="">Home</Link>
            <Link to="cart">Cart</Link>
            <Link to="/favourites">Favourites</Link>
          </nav>
          <Switch>
            />
            <Route
              path="/cart"
              render={routeProps => (
                <Cart
                  {...routeProps}
                  cartItems={cartItems}
                  className="list"
                  list={items}
                  favourites={favourites}

                />
              )}
            ></Route>
            <Route
              exact
              path="/"
              render={routeProps => (
                <ListOfItems
                  {...routeProps}
                  favourites={favourites}
                  className="list"
                  list={items}
                  isAddToCartButton={true}
                />
              )}
            ></Route>
            <Route
              path="/favourites"
              render={routeProps => (
                <Favourites
                  {...routeProps}
                  favourites={favourites}
                  list={items}
                />
              )}
            ></Route>
          </Switch>
        </div>
      )}
    </div>
  );

  return null;
}

export default App;
