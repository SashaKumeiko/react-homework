export const nameAction=(name)=>{
    return {
        type: 'NAME', payload: name
    }
}

export const toggleModalAction=()=>{
    return {type: 'IS_MODAL'}
}