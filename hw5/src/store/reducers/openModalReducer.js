const initialModal = {
  addToCartIsOpen: false,
  currentName: "no"
};

const modalReducer = (state = initialModal, action) => {
  
    switch (action.type) {
      
    case 'IS_MODAL':
      return {
        ...state,
        addToCartIsOpen: !state.addToCartIsOpen,
      };

    case 'NAME':
      return {
        ...state,
        currentName: action.payload,
      };
  }
  return state;
};
export default modalReducer;
