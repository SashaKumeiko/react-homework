import React from "react";
import ListOfItems from "./ListOfItems";

export default function Cart(props) {
  //  проверка на пустую строку
//   if (!props.cartItems[props.cartItems.length - 1]) {
//     props.cartItems.pop();
  // }  
  // ниже - другая проверка на пустую строку
  let cartItems = props.cartItems.filter((el)=>{return el!=false})

   let filteredList = cartItems.map(cartItem => {
    return props.list.find(item => {
      return item.name === cartItem;
    });
  });

    return (
    <div>
      <ListOfItems
        hasCloseButtonClass={true}
        favourites={props.favourites}
        className="list"
        list={filteredList}
 
      />
    </div>
  );
}
