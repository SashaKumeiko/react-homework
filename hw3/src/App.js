import React, { useState, useEffect } from "react";
import Button from "./Components/Buttons/Button";
import Modal from "./Components/Modals/Modal";
import ListOfItems from "./Components/Items/ListOfItems";
import Cart from "./Components/Items/Cart";
import Favourites from "./Components/Items/Favourites";
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

import "./Styles/app.scss";

function App() {
  let [items, setItems] = useState({
    items: undefined
  });

  let [cart, setCartItems] = useState({
    cartItems: []
  });

  let [favourites, setFavouriteItems] = useState({
    favouritesFromLs: []
  });

  function changeListState() {
    setCartItems({ cartItems: localStorage.getItem("cart").split(",") });
    setFavouriteItems({
      favouritesFromLs: localStorage.getItem("favourites").split(",")
    });
  }

  function removeFromCart(event) {
    let name = event.target.dataset.name;
    let array = localStorage.getItem("cart").split(",");
    let index = array.indexOf(name);
    array.splice(index, 1);

    localStorage.setItem("cart", array);
    changeListState();
  }

  function addToCart(name) {
    localStorage.setItem("cart", [localStorage.getItem("cart") + name + ","]);
    changeListState(name);
  }

  useEffect(() => {
    if (!localStorage.getItem("cart")) {
      localStorage.setItem("cart", "");
    }

    if (!localStorage.getItem("favourites")) {
      localStorage.setItem("favourites", "");
    }

    fnFetch();
    async function fnFetch() {
      await fetch("../items.json")
        .then(res => {
          return res.json()})
        .then(data => {
          
          setItems({
            items: [...data.items]
          });
          setCartItems({ cartItems: localStorage.getItem("cart").split(",") });
          setFavouriteItems({
            favouritesFromLs: localStorage.getItem("favourites").split(",")
          });
        });
    }
  }, []);

  if (items.items) {
    return (
      <div>
        <nav className="navigation">
          <Link to="">Home</Link>
          <Link to="cart">Cart</Link>
          <Link to="/favourites">Favourites</Link>
        </nav>
        <Switch>
          />
          <Route
            path="/cart"
            render={routeProps => (
              <Cart
                {...routeProps}
                cartItems={cart.cartItems}
                className="list"
                list={items.items}
                changeListState={changeListState}
                favouritesFromLs={favourites.favouritesFromLs}
                addToCart={addToCart}
                removeFromCart={removeFromCart}
              />
            )}
          ></Route>
          <Route
            exact
            path="/"
            render={routeProps => (
              <ListOfItems
                {...routeProps}
                favouritesFromLs={favourites.favouritesFromLs}
                className="list"
                list={items.items}
                changeListState={changeListState}
                addToCart={addToCart}
                isAddToCartButton={true}
              />
            )}
          ></Route>
          <Route
            path="/favourites"
            render={routeProps => (
              <Favourites
                {...routeProps}
                favouritesFromLs={favourites.favouritesFromLs}
                list={items.items}
                changeListState={changeListState}
                addToCart={addToCart}
              />
            )}
          ></Route>
        </Switch>
      </div>
    );
  }
  return null;
}

export default App;
