import React, { useState, useEffect } from "react";
import ListOfItems from "./ListOfItems";

export default function Cart(props) {
  if (!props.cartItems[props.cartItems.length - 1]) {
    props.cartItems.pop();
  }
  let filteredList = props.cartItems.map(cartItem => {
    return props.list.find(item => {
      return item.name === cartItem;
    });
  });

  return (
    <div>
      <ListOfItems
        hasCloseButtonClass={true}
        favouritesFromLs={props.favouritesFromLs}
        className="list"
        list={filteredList}
        changeListState={props.changeListState}
        favouritesFromLs={props.favouritesFromLs}
        addToCart={props.addToCart}
        removeFromCart={props.removeFromCart}
      />
    </div>
  );
}
