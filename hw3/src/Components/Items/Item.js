import React, { useState, useEffect } from "react";

import Button from "../Buttons/Button";
import Modal from "../Modals/Modal";
import "../../Styles/item.scss";

function Item(props)  {

   let [modal, setModal] = useState({
      AddToCartOpen: false
    });

 function addToCartModalToggler() {
    setModal(prevState => ({
      AddToCartOpen: !prevState.AddToCartOpen
    }));
  }

 function toggleModal(){
    addToCartModalToggler();
    props.addToCart(props.details.name)
  }

    return (
      <div className={props.className}>
        <div className="item-top">
          <span
            className={props.hasCloseButtonClass}
            onClick={props.removeFromCart}
            data-name={props.details.name}
          >
            {props.hasX}
          </span>
        </div>
        <img src={props.details.image} />

        <div className="item-name">
          <span>{props.details.name}</span>
        </div>
        <div className="color">color: {props.details.color}</div>
        <div className="favourite">
          <div>
            <span>
              <i
                className={props.starClassName}
                data-name={props.details.name}
                onClick={()=>props.addToFav(props)}
              ></i>
            </span>
          </div>
          <div>
            <span>product code: {props.details["product code"]}</span>
          </div>
        </div>

        <div className="price">
          <span>{props.details.price} UAH</span>
          {props.isAddToCartButton && <Button
            className="first-button button"
            onClick={addToCartModalToggler}
            text="add to cart"
            background={{ backgroundColor: "#00B1E1" }}
          />}
          {modal.AddToCartOpen && (
            <Modal
              className="modal-1 modal"
              header="Do you want to add this item to cart?"
              closeModal={addToCartModalToggler}
              closeButton={true}
              actions={[
                <Button
                  nameOfItem={props.details.name}
                  text="ok"
                  className="button modal-button add-to-cart-button"
                  key="1"
                  onClick={toggleModal}
                />,
                <Button
                  text="cancel"
                  className="button modal-button cancel"
                  key="2"
                  onClick={addToCartModalToggler}
                />
              ]}
              text=""
            />
          )}
        </div>
      </div>
    );
  }

export default Item;
