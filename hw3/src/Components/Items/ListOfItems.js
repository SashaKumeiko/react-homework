import React, { useState } from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";

import Item from "./Item";

import "../../Styles/listOfItems.scss";

function ListOfItems(props) {
  function addToFav(props) {
    if (props.isFavourite) {
      let name = props.details.name;
      const str = localStorage.getItem("favourites");
      let arr = str.split(",");
      let fArr = arr.filter(el => {
        return el !== name;
      });
      localStorage.setItem("favourites", fArr);
      props.changeListState(name);
    } else {
      let name = props.details.name;
      localStorage.setItem("favourites", [
        localStorage.getItem("favourites") + name + ","
      ]);

      props.changeListState(name);
    }
  }
  return (
    <div className={props.className}>
      {props.list.map((item, index) => {
        let isFavourite = props.favouritesFromLs.some(function checkName(name) {
          return name === item.name;
        });

        return (
          <Item
            hasCloseButtonClass={props.hasCloseButtonClass ? "close" : ""}
            hasX={props.hasCloseButtonClass ? "×" : ""}
            key={index}
            details={item}
            className="item"
            isFavourite={isFavourite}
            starClassName={isFavourite ? "fas fa-star colored" : "fas fa-star"}
            addToFav={addToFav}
            changeListState={props.changeListState}
            addToCart={props.addToCart}
            removeFromCart={props.removeFromCart}
            isAddToCartButton={props.isAddToCartButton}
          />
        );
      })}
    </div>
  );
}
export default ListOfItems;
