import React, { useState, useEffect } from "react";
import ListOfItems from "./ListOfItems";

export default function Favourites(props) {
  let filteredList = props.list.filter(function(item) {
    return props.favouritesFromLs.includes(item.name);
  });

  return (
    <div>
      {" "}
      <ListOfItems
        favouritesFromLs={props.favouritesFromLs}
        className="list"
        list={filteredList}
        changeListState={props.changeListState}
        addToCart={props.addToCart}
        isAddToCartButton = {true}
      />
    </div>
  );
}
