import React from "react";
import PropTypes from "prop-types";

import "../../Styles/button.scss";

function Button(props) {
  return (
    <button
      onClick={props.onClick}
      style={props.background}
      className={props.className}
      data-name={props.nameOfItem}
    >
      {" "}
      {props.text}
    </button>
  );
}
Button.propTypes = {
  background: PropTypes.object,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func
};
export default Button;
