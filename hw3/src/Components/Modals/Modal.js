import React, { useEffect } from "react";
import Button from "../Buttons/Button.js";

import PropTypes from "prop-types";

import "../../Styles/modal.scss";

function Modal(props) {
  useEffect(() => {
    let modal = document.querySelector(".modal");

    window.addEventListener("click", Close);
    function Close(event) {
        if (event.target === modal) {
          props.closeModal();
        }
    }
  }, []);

  return (
    <div className={props.className}>
      <div className="modal-content">
        <div className="modal-header">
          <span className="close" onClick={props.closeModal}>
            &times;
          </span>
          <h2>{props.header}</h2>
        </div>

        <div className="modal-body">
          <p>{props.text}</p>
        </div>

        <div className="modal-footer">
          <h3 key={props.key}>{props.actions}</h3>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array
};

export default Modal;
