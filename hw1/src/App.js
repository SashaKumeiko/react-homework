import React from "react";
import Button from "./Components/Buttons/Button";
import Modal from "./Components/Modals/Modal";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstOpen: false,
      isSecondOpen: false
    };
  }

  firstModalToggler(event) {
    //  event.preventDefault()
    this.setState(prevState => ({
      isFirstOpen: !prevState.isFirstOpen
    }));
  }

  secondModalToggler(event) {
    this.setState(prevState => ({
      isSecondOpen: !prevState.isSecondOpen
    }));
  }

  render() {
    return (
      <div>
        <Button
          className="first-button button"
          onClick={this.firstModalToggler.bind(this)}
          text="open first modal"
          background={{ backgroundColor: "#00B1E1" }}
        />
        <Button
          className="second-button button"
          onClick={this.secondModalToggler.bind(this)}
          text="open second modal"
          background={{ backgroundColor: "#37BC9B" }}
        />
        {this.state.isFirstOpen && (
          <Modal
            className="modal-1 modal"
            header="Do you want to delete this file?"
            closeModal={this.firstModalToggler.bind(this)}
            closeButton={true}
            actions={[
              <Button text="ok" className="button modal-button" />,
              <Button text="cancel" className="button modal-button" />
            ]}
            text="Once you delete this file, it won't be possible to undo this action. Are you sure, you want to delete it? "
          />
        )}
        {this.state.isSecondOpen && (
          <Modal
            className="modal-2 modal"
            header="Do ?"
            closeModal={this.secondModalToggler.bind(this)}
            closeButton={true}
            actions={[
              <Button text="ok" className="button modal-button" />,
              <Button text="ne ok" className="button modal-button" />,
              <Button text="cancel" className="button modal-button" />
            ]}
            text="Are you sure? "
          />
        )}
      </div>
    );
  }
}

export default App;
