import React from "react";
import PropTypes from "prop-types";

import "../../Styles/button.scss";

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button
        onClick={this.props.onClick}
        style={this.props.background}
        className={this.props.className}
      >
        {" "}
        {this.props.text}
      </button>
    );
  }
}
Button.propTypes = {
  background: PropTypes.object,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func
};
export default Button;
