import React from "react";
import Button from "../Buttons/Button.js";

import PropTypes from "prop-types";

import "../../Styles/modal.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(this.props.closeModal);

    let modals = document.querySelectorAll(".modal");

    window.addEventListener("click", Close.bind(this));
    function Close(event) {
      modals.forEach(modal => {
        if (event.target === modal) {
          this.props.closeModal();
        }

        let close = modal.querySelector(".close");
        close.onclick = this.props.closeModal;
      });
    }
  }

  render() {
    return (
      <div className={this.props.className}>
        <div className="modal-content">
          <div className="modal-header">
            <span className="close">&times;</span>
            <h2>{this.props.header}</h2>
          </div>

          <div className="modal-body">
            <p>{this.props.text}</p>
          </div>

          <div className="modal-footer">
            <h3>
              {this.props.actions}

            </h3>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array
};

export default Modal;
