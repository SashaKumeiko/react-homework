const initialLists = {
    cartItems:[],
    favouritesFromLs:[]
}
const listsReducer = (state = initialLists, action) => {
    switch(action.type){
        case "CART":
            return {
                ...state,
                cartItems: action.payload
              };

        case "FAVOURITES":
            return {
                ...state,
                favouritesFromLs: action.payload        
              };
    }
    return state;
}

export default listsReducer