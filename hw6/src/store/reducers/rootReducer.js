import {combineReducers} from 'redux'
import modalReducer from "./openModalReducer"
import listsReducer from "./listsReducer"
import {itemsReducer} from "./itemsReducer"

export const rootReducer = combineReducers({modalReducer,listsReducer,itemsReducer});
