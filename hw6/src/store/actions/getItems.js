export const getItems = () => {
  return dispatch => {
     fetch("../items.json")
    .then(res => {
      return res.json()})
    .then(data => {
        dispatch({
          type: 'GET_ITEMS',
          payload: [...data.items],
        });
      });
  };
};

export const cartItemsAction=()=>{
  return {
    type: 'CART',
    payload: localStorage.getItem('cart').split(','),
  }
 }; 

// export const clearCartAction=()=>{
//   return {
//     type: 'CART',
//     payload: 'as'
//   }
// } 


 export const favouritesAction=()=>{
   return {
    type: 'FAVOURITES',
    payload: localStorage.getItem('favourites').split(','),
  }

}