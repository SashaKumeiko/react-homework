import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {withFormik, Form, Field} from 'formik';
import * as yup from 'yup';
import {string, number} from 'yup';

import '../../Styles/form.scss';

function FormData({errors, touched, validateForm, ...props}) {

  return (
    <Form className="form">
      <Field
        className="field"
        name="name"
        placeholder="enter name"
      ></Field>
      {errors.name && touched.name ? <div>{errors.name}</div> : null}
      <Field
        className="field"
        name="surname"
        placeholder="enter surname"
      ></Field>
      {errors.surname && touched.surname ? <div>{errors.surname}</div> : null}
      <Field className="field" name="age" placeholder="enter age"></Field>
      {errors.age && touched.age ? <div>{errors.age}</div> : null}
      <Field
        className="field"
        name="address"
        placeholder="enter address"
      ></Field>
      {errors.address && touched.address ? <div>{errors.address}</div> : null}
      <Field className="field" name="phone" placeholder="enter phone number" />
      {errors.phone && touched.phone ? <div>{errors.phone}</div> : null}
      <button disabled={!props.isValid}
        // onClick={() =>
        //   validateForm().then(resolve => {
        //     if (
        //       Object.keys(resolve).length &&
        //       resolve.constructor === Object
        //     ) {
        //       console.log("fill the fields")
        //     } 
        //   })
        // }
      >
        Checkout
      </button>
    </Form>
  );
}
const FormikForm = withFormik({
  mapPropsToValues({}) {
    return {
      name: '',
      surname: '',
      age: '',
      address: '',
      phone: '',
    };
  },
  isInitialValid: false,
  validationSchema: yup.object().shape({
    name: string().required(),
    surname: string().required(),
    age: number()
      .required()
      .positive()
      .integer(),
    address: string().required(),
    // phone: string().matches(/^[0-9]\d{9}$/, {
    //   message: 'Please enter valid number.',
    //   excludeEmptyString: false,
    // }),
    phone: number()
    .required()
    .positive()
    .integer(),
  }),
  handleSubmit(values, {props}) {
    console.log('submited data ', values);
    console.log('purchased goods: ', props.cartItems);
    localStorage.setItem('cart', '');
    props.dispatchCartItems();
  },
})(FormData);

export default FormikForm;
