import React from 'react';
import {
  cartItemsAction,
  favouritesAction,
} from '../../store/actions/getItems';
import {useSelector, useDispatch} from 'react-redux';
import {nameAction, toggleModalAction} from '../../store/actions/modalActions';
import Button from '../Buttons/Button';
import Modal from '../Modals/Modal';

import Item from './Item';

import '../../Styles/listOfItems.scss';

function ListOfItems(props) {

  const isModal = useSelector(state => state.modalReducer.addToCartIsOpen);
  const currentName = useSelector(state => state.modalReducer.currentName);
  const dispatch = useDispatch();
  const favourites = useSelector(state => state.listsReducer.favouritesFromLs);
  const cartItems = useSelector(state => state.listsReducer.cartItems);

  function removeFromCart(event) {
    let name = event.target.dataset.name;
    let array = cartItems;
    let index = array.indexOf(name);
    array.splice(index, 1);
    localStorage.setItem('cart', array);
    dispatch(cartItemsAction());
  }

  function addToCart(name) {
    if (localStorage.getItem('cart')) {
      localStorage.setItem('cart', [localStorage.getItem('cart') + ',' + name]);
    } else {
      localStorage.setItem('cart', [localStorage.getItem('cart') + name]);
    }
    dispatch(cartItemsAction());
  }

  function toggleModal() {
    dispatch(toggleModalAction());
    addToCart(currentName);
  }

  function checkModal(propsFromItem) {
    dispatch(nameAction(propsFromItem.details.name));
    dispatch(toggleModalAction());
  }

  function closeModal() {
    dispatch(toggleModalAction());
  }

  function addToFav(props) {
    if (props.isFavourite) {
      let name = props.details.name;
      let arr = favourites;
      let fArr = arr.filter(el => {
        return el !== name;
      });
      localStorage.setItem('favourites', fArr);
      dispatch(favouritesAction());
    } else {
      let name = props.details.name;
      if (localStorage.getItem('favourites')) {
        localStorage.setItem('favourites', [
          localStorage.getItem('favourites') + ',' + name,
        ]);
      } else {
        localStorage.setItem('favourites', [
          localStorage.getItem('favourites') + name,
        ]);
      }

      dispatch(favouritesAction());
    }
  }
  return (
    <div className={props.className}>
      {isModal && (
        <Modal
          className="modal-1 modal"
          header="Do you want to add this item to cart?"
          closeModal={closeModal}
          closeButton={true}
          actions={[
            <Button
              nameOfItem={props.currentName}
              text="ok"
              className="button modal-button add-to-cart-button"
              key="1"
              onClick={toggleModal}
            />,
            <Button
              text="cancel"
              className="button modal-button cancel"
              key="2"
              onClick={closeModal}
            />,
          ]}
          text=""
        />
      )}
      {props.list.map((item, index) => {
        let isFavourite = props.favourites.some(function checkName(name) {
          return name === item.name;
        });

        return (
          <Item
            setName={props.setName}
            checkModal={checkModal}
            changeModal={props.changeModal}
            isModal={isModal}
            hasCloseButtonClass={props.hasCloseButtonClass ? 'close' : ''}
            hasX={props.hasCloseButtonClass ? '×' : ''}
            key={index}
            details={item}
            className="item"
            isFavourite={isFavourite}
            starClassName={isFavourite ? 'fas fa-star colored' : 'fas fa-star'}
            addToFav={addToFav}
            addToCart={addToCart}
            removeFromCart={removeFromCart}
            isAddToCartButton={props.isAddToCartButton}
          />
        );
      })}
    </div>
  );
}

export default ListOfItems;
