import React from 'react';
import ListOfItems from './ListOfItems';
import FormikForm from './Form'
import {useDispatch, useSelector} from 'react-redux';
import {cartItemsAction} from '../../store/actions/getItems';


export default function Cart(props) {
  const dispatch  = useDispatch()
  console.log(props.cartItems)

  function dispatchCartItems(){
    dispatch(cartItemsAction())
  }
  //  проверка на пустую строку
  //   if (!props.cartItems[props.cartItems.length - 1]) {
  //     props.cartItems.pop();
  // }
  // ниже - другая проверка на пустую строку
  let cartItems = props.cartItems.filter(el => {
    return el != false;
  });

  let filteredList = cartItems.map(cartItem => {
    return props.list.find(item => {
      return item.name === cartItem;
    });
  });

  return (
    <div>
      <FormikForm  cartItems={cartItems} dispatchCartItems={dispatchCartItems}/>
      <ListOfItems
        hasCloseButtonClass={true}
        favourites={props.favourites}
        className="list"
        list={filteredList}
      />
    </div>
  );
}
