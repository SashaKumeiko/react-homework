import React from 'react';
import {shallow} from 'enzyme';
import Modal from '../Components/Modals/Modal';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import Button from '../Components/Buttons/Button'

const mockStore = configureStore(); 

describe('With React Testing Library', () => {
  const initialState = {output: 10};
  const mockStore = configureStore();
  let store;
  const props = {
    closeModal: true,
    header: 'text',
    text: "text2"
  };
  it('test component', () => {
    store = mockStore(initialState);
    const ModalComponent = shallow(
      <Provider store={store}>
        <Modal closeModal={props.closeModal} header={props.header} text={props.text} />
      </Provider>
    );
    expect(ModalComponent).toMatchSnapshot();
    expect(ModalComponent.props().children.props).toEqual({
      closeModal: true, 
      header: 'text',
      text: "text2", 
    });
  });
});



 