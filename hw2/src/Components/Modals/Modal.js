import React from "react";
import Button from "../Buttons/Button.js";

import PropTypes from "prop-types";

import "../../Styles/modal.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

    let modals = document.querySelectorAll(".modal");

    window.addEventListener("click", Close.bind(this));
    function Close(event) {
      modals.forEach(modal => {
        if (event.target === modal) {
          this.props.closeModal();
        }
      });
    }
    
  }

  render() {
    return (
      <div className={this.props.className}>
        <div className="modal-content">
          <div className="modal-header">
            <span className="close" onClick ={this.props.closeModal}>&times;</span>
            <h2>{this.props.header}</h2>
          </div>

          <div className="modal-body">
            <p>{this.props.text}</p>
          </div>

          <div className="modal-footer">
            <h3 key={this.props.key}>{this.props.actions}</h3>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array
};

export default Modal;
