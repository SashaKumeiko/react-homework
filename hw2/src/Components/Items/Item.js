import React from "react";
import Button from "../Buttons/Button";
import Modal from "../Modals/Modal";
import "../../Styles/item.scss";

class Item extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      AddToCartOpen: false
    };
  }

  addToCartModalToggler(event) {
    this.setState(prevState => ({
      AddToCartOpen: !prevState.AddToCartOpen
    }));
  }
  addToCart(event) {
    let name = event.target.dataset.name;
    localStorage.setItem("cart", [localStorage.getItem("cart") + name + ","]);

    this.addToCartModalToggler();
  }

  render() {
    return (
      <div className={this.props.className}>
        <img src={this.props.details.image} />

        <div className="item-name">
          <span>{this.props.details.name}</span>
        </div>
        <div className="color">color: {this.props.details.color}</div>
        <div className="favourite">
          <div>
            <span>
              <i
                className={this.props.starClassName}
                data-name={this.props.details.name}
                onClick={this.props.addToFav.bind(this)}
              ></i>
            </span>
          </div>
          <div>
            <span>product code: {this.props.details["product code"]}</span>
          </div>
        </div>

        <div className="price">
          <span>{this.props.details.price} UAH</span>
          <Button
            className="first-button button"
            onClick={this.addToCartModalToggler.bind(this)}
            text="add to cart"
            background={{ backgroundColor: "#00B1E1" }}
          />
          {this.state.AddToCartOpen && (
            <Modal
              className="modal-1 modal"
              header="Do you want to add this item to cart?"
              closeModal={this.addToCartModalToggler.bind(this)}
              closeButton={true}
              actions={[
                <Button
                  nameOfItem={this.props.details.name}
                  text="ok"
                  className="button modal-button add-to-cart-button"
                  key="1"
                  onClick={this.addToCart.bind(this)}
                />,
                <Button
                  text="cancel"
                  className="button modal-button cancel"
                  key="2"
                  onClick={this.addToCartModalToggler.bind(this)}
                />
              ]}
              text=""
            />
          )}
        </div>
      </div>
    );
  }
}
export default Item;
