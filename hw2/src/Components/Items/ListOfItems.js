import React from "react";
import Item from "./Item";

import "../../Styles/listOfItems.scss";

class ListOfItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      favouritesFromLs: localStorage.getItem("favourites").split(",") || []
    };
  }

  changeListState() {
    this.setState({
      favouritesFromLs: localStorage.getItem("favourites").split(",")
    });
  }

  addToFav(event) {
    if (this.props.isFavourite) {
      let name = event.target.dataset.name;
      const str = localStorage.getItem("favourites");
      let arr = str.split(",");
      let fArr = arr.filter(el => {
        return el !== name;
      });
      localStorage.setItem("favourites", fArr);
      this.props.changeListState();
    } else {
      let name = event.target.dataset.name;

      localStorage.setItem("favourites", [
        localStorage.getItem("favourites") + name + ","
      ]);
      this.props.changeListState();
    }
  }
  render() {
    return (
      <div className={this.props.className}>
        {this.props.list.map((item, index) => {
          let isFavourite = this.state.favouritesFromLs.some(function checkName(
            name
          ) {
            return name === item.name;
          });

          return (
            <Item
              key={index}
              details={item}
              className="item"
              isFavourite={isFavourite}
              starClassName={
                isFavourite ? "fas fa-star colored" : "fas fa-star"
              }
              addToFav={this.addToFav}
              changeListState={this.changeListState.bind(this)}
            />
          );
        })}
      </div>
    );
  }
}
export default ListOfItems;
