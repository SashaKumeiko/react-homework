import React from "react";
import Button from "./Components/Buttons/Button";
import Modal from "./Components/Modals/Modal";
import ListOfItems from "./Components/Items/ListOfItems";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: undefined
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem("cart")) {
      localStorage.setItem("cart", "");
    }

    if (!localStorage.getItem("favourites")) {
      localStorage.setItem("favourites", "");
    }

    await fetch("../items.json")
      .then(res => res.json())
      .then(data => {
        this.setState({ items: [...data.items] });
      });
  }

  render() {
    if (this.state.items) {
      return (
        <div>
          <ListOfItems
            className="list"
            list={this.state.items}
          />
        </div>
      );
    }
    return null;
  }
}

export default App;
